#Distributed Systems and Security - Coursework 1

##Set up

###Requirements
- Java 8
- Wildfly 10
- Maven 3.x

###Deployment
- Go to the Wildfly standalone root directory, create a user and start the server with
```
bin/adduser.sh # If necessary
bin/standalone.sh -c standalone-full.xml
```

- From the root of this project, run:
```
mvn clean install wildfly:deploy
```

- Access the project at <http://localhost:8080/wildfly-helloworld-mdb/>
