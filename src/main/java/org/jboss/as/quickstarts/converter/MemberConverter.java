package org.jboss.as.quickstarts.converter;

import org.jboss.as.quickstarts.entity.Member;
import org.jboss.as.quickstarts.exception.MemberNotFoundException;
import org.jboss.as.quickstarts.manager.MemberManager;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

@Named
public class MemberConverter implements Converter {

    @Inject
    private MemberManager memberManager;

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        try {
            System.out.println("Finding Member");
            Member m = memberManager.findByUsername(s);
            System.out.println(m.toString());
            return m;
        } catch (MemberNotFoundException e) {
            throw new IllegalStateException();
        }
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        if (o instanceof Member) {
            return ((Member)o).getUsername();
        } else {
            throw new IllegalStateException();
        }
    }
}
