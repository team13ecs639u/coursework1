package org.jboss.as.quickstarts.filter;

import org.jboss.as.quickstarts.entity.Member;
import org.jboss.as.quickstarts.exception.AuthTokenNotFoundException;
import org.jboss.as.quickstarts.manager.AuthTokenManager;

import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AuthFilter implements Filter {

    @Inject
    private AuthTokenManager authTokenManager;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpSession session = ((HttpServletRequest)servletRequest).getSession();
        String token = (String)session.getAttribute("auth_token");
        Member member = (Member)session.getAttribute("member");

        if (!this.authTokenManager.isValidAuthToken(token, member)) {
            String contextPath = ((HttpServletRequest)servletRequest).getContextPath();
            ((HttpServletResponse)servletResponse).sendRedirect(String.format("%s/auth/login.xhtml", contextPath));
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
    }


}
