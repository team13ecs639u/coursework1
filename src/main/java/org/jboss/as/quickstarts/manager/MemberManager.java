package org.jboss.as.quickstarts.manager;

import org.jboss.as.quickstarts.entity.Member;
import org.jboss.as.quickstarts.exception.MemberNotFoundException;

import javax.ejb.Stateless;
import javax.persistence.*;
import java.util.List;

@Stateless
public class MemberManager {

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Saves a Member
     * @param member the Member to save
     * @return Member
     */
    public Member save(Member member) {

        if (!this.entityManager.contains(member)) {
            member = this.entityManager.merge(member);
        }
        this.entityManager.persist(member);

        return member;
    }

    /**
     * Returns a Member given by its username
     * @param username the username to search for
     * @return Member
     * @throws MemberNotFoundException
     */
    public Member findByUsername(String username) throws MemberNotFoundException {

        Query q = this.entityManager.createNamedQuery("Member.findByUsername");
        q.setParameter("username", username);

        try {
            return (Member)q.getSingleResult();
        } catch (NoResultException e) {
            throw new MemberNotFoundException();
        }

    }

    /**
     * Returns all of the members that a given member is allowed to see
     * @param member Member
     * @return List<Member>
     */
    public List<Member> findAllForMember(Member member) {
        Query q = this.entityManager.createNamedQuery("Member.findAllForMember");
        q.setParameter("member", member);

        return (List<Member>)q.getResultList();
    }
}
