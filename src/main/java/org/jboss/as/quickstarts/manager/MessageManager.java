package org.jboss.as.quickstarts.manager;

import org.jboss.as.quickstarts.entity.Member;
import org.jboss.as.quickstarts.entity.Message;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class MessageManager {

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Saves a given Message
     * @param message the Message to save
     * @return Message
     */
    public Message save(Message message) {
        this.entityManager.persist(message);

        return message;
    }

    /**
     * Returns all of the Received Messages for a Member
     * @param member the Member
     * @return List<Message>
     */
    public List<Message> findByRecipient(Member member) {
        Query q = this.entityManager.createNamedQuery("Message.findAllByReceivingMember");
        q.setParameter("member", member);

        return (List<Message>)q.getResultList();
    }

    /**
     * Returns all of the Sent Messages for a Member
     * @param member the Member
     * @return List<Message>
     */
    public List<Message> findBySender(Member member) {
        Query q = this.entityManager.createNamedQuery("Message.findAllBySendingMember");
        q.setParameter("member", member);

        return (List<Message>)q.getResultList();
    }
}
