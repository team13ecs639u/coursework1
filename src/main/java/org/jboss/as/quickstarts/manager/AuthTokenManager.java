package org.jboss.as.quickstarts.manager;

import org.jboss.as.quickstarts.entity.AuthToken;
import org.jboss.as.quickstarts.entity.Member;
import org.jboss.as.quickstarts.exception.AuthTokenNotFoundException;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class AuthTokenManager {

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Saves an AuthToken
     * @param authToken the auth token to save
     * @return AuthToken
     */
    public AuthToken save(AuthToken authToken) {
        this.entityManager.persist(authToken);

        return authToken;
    }

    /**
     * Returns an AuthToken given by the token and Member
     * @param token the String representation of the token
     * @param member the Member
     * @return AuthToken
     * @throws AuthTokenNotFoundException
     */
    public AuthToken findByAuthTokenTokenAndMember(String token, Member member) throws AuthTokenNotFoundException {
        Query q = this.entityManager.createNamedQuery("AuthToken.findByTokenAndMember");
        q.setParameter("token", token);
        q.setParameter("member", member);

        try {
            return (AuthToken) q.getSingleResult();
        } catch (NoResultException e) {
            throw new AuthTokenNotFoundException();
        }
    }

    /**
     * Returns whether or not a given token is valid
     * @param token the String representation of a token
     * @param member the Member
     * @return boolean
     */
    public boolean isValidAuthToken(String token, Member member) {
        if (token == null || token.length() < 1) {
            return false;
        }

        try {
            this.findByAuthTokenTokenAndMember(token, member);

            return true;
        } catch(AuthTokenNotFoundException e) {
            return false;
        }
    }
}
