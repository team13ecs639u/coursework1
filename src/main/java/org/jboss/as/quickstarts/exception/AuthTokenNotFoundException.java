package org.jboss.as.quickstarts.exception;

public class AuthTokenNotFoundException extends Exception {

    public AuthTokenNotFoundException() {
        super("Auth Token not Found.");
    }
}
