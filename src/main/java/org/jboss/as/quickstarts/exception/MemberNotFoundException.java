package org.jboss.as.quickstarts.exception;

public class MemberNotFoundException extends Exception {

    public MemberNotFoundException() {
        super("Member not Found");
    }
}
