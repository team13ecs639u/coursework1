package org.jboss.as.quickstarts.provider;


import org.jboss.as.quickstarts.entity.Member;
import org.jboss.as.quickstarts.entity.Message;

import java.io.Serializable;
import java.util.Date;

public class MessageProvider implements Serializable {

    /**
     * Returns a new Message
     * @param sender the sending Member
     * @param recipient the receiving Member
     * @param message the message
     * @return Message
     */
    public Message createFromSenderRecipientAndMessage(
            Member sender,
            Member recipient,
            String message
    ) {
        Message m = new Message();

        m.setSender(sender);
        m.setRecipient(recipient);
        m.setMessage(message);

        m.setTimestamp(new Date());

        return m;
    }

}
