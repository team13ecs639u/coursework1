package org.jboss.as.quickstarts.provider;

import org.jboss.as.quickstarts.entity.Member;


public class MemberProvider {

    /**
     * creates a new Member
     * @param username the username
     * @param password the password
     * @return Member
     */
    public Member createFromUsernameAndPassword(String username, String password) {
        Member member = new Member();

        member.setUsername(username);
        member.setPassword(password);

        return member;
    }
}
