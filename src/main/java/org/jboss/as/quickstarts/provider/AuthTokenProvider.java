package org.jboss.as.quickstarts.provider;


import org.jboss.as.quickstarts.entity.AuthToken;
import org.jboss.as.quickstarts.entity.Member;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.SecureRandom;


public class AuthTokenProvider implements Serializable {

    private SecureRandom secureRandom = new SecureRandom();

    /**
     * See: http://stackoverflow.com/a/41156
     *
     * @param member the member to generate an AuthToken for
     * @return AuthToken
     */
    public AuthToken createFromMember(Member member) {
        String token = new BigInteger(130, secureRandom).toString(32);

        AuthToken authToken = new AuthToken();
        authToken.setToken(token);
        authToken.setMember(member);

        return authToken;
    }

}
