package org.jboss.as.quickstarts.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@NamedQueries({
        @NamedQuery(
                name = "Message.findAllByReceivingMember",
                query = "SELECT m from Message m WHERE m.recipient = :member ORDER BY m.timestamp DESC"
        ),
        @NamedQuery(
                name = "Message.findAllBySendingMember",
                query = "SELECT m from Message m WHERE m.sender = :member ORDER BY m.timestamp DESC"
        )
})
public class Message implements Serializable {

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ManyToOne
    private Member recipient;

    @ManyToOne
    private Member sender;

    @NotNull
    private String message;

    @NotNull
    private Date timestamp;

    /**
     * Returns the Id
     * @return long
     */
    public long getId() {
        return this.id;
    }

    /**
     * Returns the recipient
     * @return Member
     */
    public Member getRecipient() {
        return this.recipient;
    }

    /**
     * Sets the recipient
     * @param member the Member to set as the recipient
     */
    public void setRecipient(Member member) {
        this.recipient = member;
    }

    /**
     * Returns the sender
     * @return Member
     */
    public Member getSender() {
        return this.sender;
    }

    /**
     * Sets the sender
     * @param member The Member to set as the sender
     */
    public void setSender(Member member) {
        this.sender = member;
    }

    /**
     * Returns the message
     * @return String
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * Sets the message
     * @param message the message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Sets the timestamp
     * @param timestamp the timestamp
     */
    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Returns the timestamp
     * @return Date
     */
    public Date getTimestamp() {
        return this.timestamp;
    }

    /**
     * Returns a humanly-readable representation of the timestamp
     * @return String
     */
    public String getReadableTimestamp() {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy h:mm:ss a");
        return sdf.format(this.getTimestamp());
    }

    public String toString() {
        return String.format(
                "Message(Id: %d, Sender: %s, Recipient: %s, Message: %s, Timestamp: %s)",
                this.getId(),
                this.getSender(),
                this.getRecipient(),
                this.getMessage(),
                this.getReadableTimestamp()
        );
    }
}
