package org.jboss.as.quickstarts.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@NamedQueries({
        @NamedQuery(
                name = "AuthToken.findByTokenAndMember",
                query = "SELECT at from AuthToken at WHERE at.token = :token AND at.member = :member"
        )
})
public class AuthToken implements Serializable {

    @NotNull
    @Size(min = 1, max = 255)
    @Id
    private String token;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    private Member member;

    /**
     * Returns the token
     * @return String
     */
    public String getToken() {
        return this.token;
    }

    /**
     * Sets the token
     * @param token the value of the token
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * Returns the Member the Token is assigned to
     * @return Member
     */
    public Member getMember() {
        return this.member;
    }

    /**
     * Sets the Member the Token is assigned to
     * @param member the Member to assign the token to
     */
    public void setMember(Member member) {
        this.member = member;
    }

    public String toString() {
        return String.format("AuthToken(username: %s, token: %s)",
                this.getMember().getUsername(),
                this.getToken()
        );
    }
}
