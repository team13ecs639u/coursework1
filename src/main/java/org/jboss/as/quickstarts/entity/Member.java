package org.jboss.as.quickstarts.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@NamedQueries({
        @NamedQuery(name = "Member.findAll", query = "SELECT m from Member m"),
        @NamedQuery(name = "Member.findByUsername", query = "SELECT m from Member m WHERE m.username = :username"),
        @NamedQuery(name = "Member.findAllLoggedIn", query = "SELECT m from Member m WHERE m.loggedIn = true"),
        @NamedQuery(
                name = "Member.findAllForMember",
                query = "SELECT m from Member m WHERE m <> :member " +
                        "AND m.loggedIn = true " +
                        "AND m.visibility <> 2 " +
                        "AND (m.visibility = 0) " +
                        "OR (m.visibility = 1 AND :member IN (SELECT a FROM m.friends a))"
        )
})
public class Member implements Serializable {

    public static int EVERYONE = 0;
    public static int FRIENDS_ONLY = 1;
    public static int NOBODY = 2;

    @NotNull
    @Size(min = 1, max = 255)
    @Id
    private String username;

    @NotNull
    @Size(min = 1, max = 255)
    private String password;

    @NotNull
    private boolean loggedIn = false;

    @NotNull
    private int visibility = Member.EVERYONE;

    @OneToMany(fetch = FetchType.EAGER)
    private Set<Member> friends = new HashSet<>();

    /**
     * Returns the username
     * @return String
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * Sets the username
     * @param username The username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Returns the password
     * @return String
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * Sets the password
     * @param password The password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Returns whether or not the Member is logged in
     * @return boolean
     */
    public boolean isLoggedIn() {
        return this.loggedIn;
    }

    /**
     * Sets whether or not the Member is logged in
     * @param loggedIn True if the member is logged in, false otherwise
     */
    public void setLoggedIn(Boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    /**
     * Returns the Visibility level of the Member
     * @return int
     */
    public int getVisibility() {
        return this.visibility;
    }

    /**
     * Returns the possible Visibility levels for a Member
     * @return int[]
     */
    public static int[] getVisibilities() {
        return new int[]{Member.EVERYONE, Member.FRIENDS_ONLY, Member.NOBODY};
    }

    /**
     * Returns a human-readable string representation of the Visbility
     * @return String
     */
    public String getReadableVisibility() {
        switch (this.getVisibility()) {
            case 0:
                return "Everyone";
            case 1:
                return "Friends Only";
            case 2:
                return "Nobody";
            default:
                return "N/A";
        }
    }

    /**
     * Sets the visibility for the Member
     * @param visibility The integer representation of a Visibility
     */
    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    /**
     * Returns the Friends
     * @return Set<Member>
     */
    public Set<Member> getFriends() {
        return this.friends;
    }

    /**
     * Adds a given Member as a Friend to the Member
     * @param friend the Member to add
     */
    public void addFriend(Member friend) {
        this.friends.add(friend);
    }

    /**
     * Removes a given Member as a Friend from the Member
     * @param friend the Member to remove
     */
    public void removeFriend(Member friend) {
        this.friends.remove(friend);
    }

    public String toString() {
        return String.format(
                "Member(username: %s, loggedIn: %s)",
                this.getUsername(),
                this.isLoggedIn()
        );
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) return true;
        if (!(object instanceof Member)) return false;

        Member other = (Member) object;

        return other.getUsername().equals(this.getUsername());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + this.username.hashCode();
        return hash;
    }

    /**
     * Serialize the Member without Friends to solve recursive serialization
     * @param out the output stream
     * @throws IOException
     */
    private void writeObject(java.io.ObjectOutputStream out) throws IOException {
        out.writeObject(this.getUsername());
        out.writeObject(this.isLoggedIn());
        out.writeObject(this.getVisibility());
    }

    /**
     * Serialize the Member without Friends to solve recursive serialization
     * @param in the input stream
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        this.username = (String) in.readObject();
        this.loggedIn = (boolean) in.readObject();
        this.visibility = (int) in.readObject();
    }
}
