/*
 * JBoss, Home of Professional Open Source
 * Copyright 2015, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jboss.as.quickstarts.mdb;

import org.jboss.as.quickstarts.controller.ChatController;
import org.jboss.as.quickstarts.manager.MessageManager;
import org.jboss.as.quickstarts.provider.MessageProvider;

import java.util.logging.Logger;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.*;

/**
 * <p>
 * A simple Message Driven Bean that asynchronously receives and processes the messages that are sent to the queue.
 * </p>
 *
 * @author Serge Pagop (spagop@redhat.com)
 *
 */
@MessageDriven(name = "HelloWorldQueueMDB", activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "queue/HELLOWORLDMDBQueue"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
    @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge") })
public class HelloWorldQueueMDB implements MessageListener {

    private final static Logger LOGGER = Logger.getLogger(HelloWorldQueueMDB.class.toString());

    @Inject
    private MessageManager messageManager;

    @Inject
    private MessageProvider messageProvider;

    /**
     * @see MessageListener#onMessage(Message)
     */
    public void onMessage(Message rcvMessage) {
        LOGGER.info(String.format("Received from queue: %s", rcvMessage));
        try {
            if (rcvMessage instanceof ObjectMessage) {
                LOGGER.info(String.format("Received ObjectMessage from queue: %s", rcvMessage));

                if (((ObjectMessage)rcvMessage).getObject() instanceof org.jboss.as.quickstarts.entity.Message) {
                    org.jboss.as.quickstarts.entity.Message msg = (org.jboss.as.quickstarts.entity.Message) ((ObjectMessage)rcvMessage).getObject();
                    LOGGER.info(String.format("Received message from queue: %s", msg));

                    msg = this.messageManager.save(msg);

                    LOGGER.info(String.format("Saved new: %s", msg));
                } else {
                    LOGGER.warning("Message of wrong type: " + rcvMessage.getClass().getName());
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
