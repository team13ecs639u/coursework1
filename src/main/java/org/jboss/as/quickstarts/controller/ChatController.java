package org.jboss.as.quickstarts.controller;

import org.jboss.as.quickstarts.entity.*;
import org.jboss.as.quickstarts.manager.MemberManager;
import org.jboss.as.quickstarts.manager.MessageManager;
import org.jboss.as.quickstarts.provider.MessageProvider;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jms.JMSContext;
import javax.jms.JMSDestinationDefinition;
import javax.jms.JMSDestinationDefinitions;
import javax.jms.Queue;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

@JMSDestinationDefinitions(
        value = {
                @JMSDestinationDefinition(
                        name = "java:/queue/HELLOWORLDMDBQueue",
                        interfaceName = "javax.jms.Queue",
                        destinationName = "HelloWorldMDBQueue"
                ),
                @JMSDestinationDefinition(
                        name = "java:/topic/HELLOWORLDMDBTopic",
                        interfaceName = "javax.jms.Topic",
                        destinationName = "HelloWorldMDBTopic"
                )
        })
@Named("chatController")
@RequestScoped
public class ChatController implements Serializable {

    @Inject
    private JMSContext context;

    @Inject
    private MessageProvider messageProvider;

    @Inject
    private MemberManager memberManager;

    @Inject
    private MessageManager messageManager;

    @Resource(lookup = "java:/queue/HELLOWORLDMDBQueue")
    private Queue queue;

    private Member currentMember = (Member) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("member");

    private Member recipient = currentMember;

    /**
     * Returns the current Logged In Member
     * @return Member
     */
    public Member getCurrentMember() {
        return this.currentMember;
    }

    /**
     * Returns the Members that the current logged in member is allowed to see
     * @return List<Member>
     */
    public List<Member> getMembers() {
        return this.memberManager.findAllForMember(this.currentMember);
    }

    /**
     * Returns the current recipient
     * @return Member
     */
    public Member getRecipient() {
        return this.recipient;
    }

    /**
     * Sets the current recipient
     * @param member The member to be the recipient
     */
    public void setRecipient(Member member) {
        this.recipient = member;
    }

    /**
     * Returns the Messages that the currentMember has received
     * @return List<Message>
     */
    public List<Message> getReceivedMessages() {
        return this.messageManager.findByRecipient(this.currentMember);
    }

    /**
     * Returns the Messages that the currentMember has sent
     * @return List<Message>
     */
    public List<Message> getSentMessages() {
        return this.messageManager.findBySender(this.currentMember);
    }

    /**
     * Returns the Visibilities tha a Member can have
     * @return int[]
     */
    public int[] getVisibilities() {
        return Member.getVisibilities();
    }

    /**
     * Sends a Message
     * @throws IOException
     */
    public void doSend() throws IOException {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String message = request.getParameter("chatForm:messageField");

        System.out.println(String.format("Sender: %s\nRecipient: %s\nMessage: %s",
                this.currentMember,
                this.recipient,
                message
        ));
        Message objectMessage = this.messageProvider.createFromSenderRecipientAndMessage(
                currentMember,
                this.recipient,
                message
        );

        this.context.createProducer().send(this.queue, objectMessage);

        this.redirect(request, "chat/index.jsf");
    }

    /**
     * Saves the currentMember - used when updating their visibility
     */
    public void doChangeVisibility() {
        this.memberManager.save(this.currentMember);
    }

    /**
     * Adds a Member to the Member's friends set
     * @param member the Member to become friends with
     */
    public void doAddFriend(Member member) {
        this.currentMember.addFriend(member);

        this.currentMember = this.memberManager.save(this.currentMember);
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("member", this.currentMember);
    }

    /**
     * Removes a Member from the Member's friends set
     * @param member the Member to remove
     */
    public void doRemoveFriend(Member member) {
        this.currentMember.removeFriend(member);

        this.currentMember = this.memberManager.save(this.currentMember);
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("member", this.currentMember);
    }

    /**
     * Redirects to a given location
     * @param request the current request
     * @param location the location to redirect to
     * @throws IOException
     */
    private void redirect(HttpServletRequest request, String location) throws IOException {
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        String contextPath = request.getContextPath();

        response.sendRedirect(String.format("%s/%s", contextPath, location));
    }
}
