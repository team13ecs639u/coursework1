package org.jboss.as.quickstarts.controller;


import org.jboss.as.quickstarts.entity.AuthToken;
import org.jboss.as.quickstarts.entity.Member;
import org.jboss.as.quickstarts.exception.MemberNotFoundException;
import org.jboss.as.quickstarts.manager.AuthTokenManager;
import org.jboss.as.quickstarts.manager.MemberManager;
import org.jboss.as.quickstarts.provider.AuthTokenProvider;
import org.jboss.as.quickstarts.provider.MemberProvider;

import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

@Named("authController")
@SessionScoped
public class AuthController implements Serializable {

    @Inject
    private MemberManager memberManager;

    @Inject
    private MemberProvider memberProvider;

    @Inject
    private AuthTokenProvider authTokenProvider;

    @Inject
    private AuthTokenManager authTokenManager;

    /**
     * Registers a Member when submitted from the registerForm
     * @throws IOException
     */
    public void doRegister() throws IOException {

        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String username = request.getParameter("registerForm:usernameField");
        String password = request.getParameter("registerForm:passwordField");

        Member member = this.memberProvider.createFromUsernameAndPassword(username, password);

        member = this.memberManager.save(member);

        System.out.println(String.format("Registered: %s", member));

        this.redirect(request, "auth/login.jsf");
    }

    /**
     * Logs in a Member when submitted from the loginForm. Sets the Member in the Session along with a AuthToken
     * @throws IOException
     */
    public void doLogin() throws IOException {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String username = request.getParameter("loginForm:usernameField");
        String password = request.getParameter("loginForm:passwordField");

        try {
            Member member = this.memberManager.findByUsername(username);

            System.out.println(String.format("Found: %s", member));

            if (password.equals(member.getPassword())) {
                // create AuthToken
                AuthToken authToken = this.authTokenProvider.createFromMember(member);
                this.authTokenManager.save(authToken);

                // Log in
                member.setLoggedIn(true);
                this.memberManager.save(member);

                // set AuthToken and Member in Session
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("auth_token", authToken.getToken());
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("member", member);
                System.out.println(String.format("Logged in as: %s", member));

                this.redirect(request, "chat/index.jsf");
            } else {
                System.out.println("Incorrect Password.");
            }

        } catch (MemberNotFoundException e) {
            System.out.println(String.format("Member Not Found: %s", username));
        }
    }

    /**
     * Logs out the current Logged In Member
     * @throws IOException
     */
    public void doLogout() throws IOException {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();

        String token = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("auth_token");
        Member member = (Member) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("member");

        if (this.authTokenManager.isValidAuthToken(token, member)) {
            System.out.println(String.format("Logging out: %s", member));
            member.setLoggedIn(false);
            this.memberManager.save(member);
        }

        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("auth_token");
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("member");

        this.redirect(request, "index.jsf");
    }

    /**
     * Redirects to a given location
     * @param request the current request
     * @param location the location to redirect to
     * @throws IOException
     */
    private void redirect(HttpServletRequest request, String location) throws IOException {
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        String contextPath = request.getContextPath();

        response.sendRedirect(String.format("%s/%s", contextPath, location));
    }

}
